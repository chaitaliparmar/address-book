document.onload = getContactDetails();

function getContactDetails(){
    const xhr = new XMLHttpRequest();
    
    xhr.open('GET', 'data.json', true);
    
    xhr.onload = function(){
        if(this.status === 200){
            const contacts = JSON.parse(this.responseText);
            let output = "";
            contacts.forEach(function(contact){
                output += `<tr>
                    <td><img class="circle" src="images/users/${contact.image_name}" alt="" height="60%"></td>
                    <td>${contact.first_name}  ${contact.last_name}</td>
                    <td>${contact.email}</td>
                    <td>${contact.birthdate}</td>
                    <td>${contact.telephone}</td>
                    <td>${contact.address}</td>
                    <td><a class="btn btn-floating green lighten-2"><i class="material-icons">edit</i></a></td>
                    <td><a class="btn btn-floating red lighten-2 modal-trigger" href="#deleteModal"><i class="material-icons">delete_forever</i></a>
                    </td>
                </tr> `;
            });
            
            
            document.getElementById('contact-details').innerHTML = output;
        }
    }
    
    xhr.send();
}