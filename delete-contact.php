<?php
require_once("includes/functions.php");
if(isset($_GET['id'])){
    $id = $_GET['id'];
    //Fetch image name and delete the image
    $row = db_select("SELECT * FROM contacts WHERE id = {$id}");
    if($row == false){
        $error = "Sorry the record which you are trying to find does not exists!";
        header("Location: index.php?q=error&op=del");
        dd($error);
    }

    $image_name = $row[0]['image_name'];
    unlink("images/users/{$image_name}");

    //DELETE the Actual record!
    $result = db_query("DELETE FROM contacts WHERE id = {$id}");
    if(!$result)
    {
        header("Location: index.php?q=error&op=del");
    }
    else
    {
        header("Location: index.php?q=success&op=del");
    }
}